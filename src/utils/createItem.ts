import { sendRequest } from "./serverInstance";

export const createItem = async (data: any)=> {
  const {category,color,gender} = data

  try {
  const reqQuery = `category=${category}&color=${color}&gender=${gender}`
    const response = await sendRequest(reqQuery)
    const base64Image = btoa(
      new Uint8Array(response?.data).reduce(
        (data, byte) => data + String.fromCharCode(byte),
        ''
      )
    );
    const src = `data:${response?.headers['content-type']};base64,${base64Image}`;
    return src
  } catch (error) {
    console.log("error ", error);
  }
};
