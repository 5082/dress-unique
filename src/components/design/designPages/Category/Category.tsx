import { Box, Button, Card, CardActionArea, CardContent } from "@mui/material";
import { DesignSectionProps } from "../../../../types/designSection";
import useUserData from "../../../../hooks/useUserData";
import { categoryOptions } from "./categoryOptions";
import { useState } from "react";
import './button.css';

export const Category = ({ saveChoice }: DesignSectionProps) => {
  const { setCategoryFeature, getUserChoices } = useUserData();
  const {gender: chosenGender} = getUserChoices()
  const [categories, setCategories] = useState(categoryOptions.filter(opt => opt.genders.includes(chosenGender as string)));

  const handleClick = (selectedCategory: string) => {
    const category = categories.find(category  => category.name === selectedCategory);
    if (!(category?.subCategories)) {
      setCategoryFeature(selectedCategory);
      saveChoice("category");
    } else {
      setCategories(category.subCategories);
    }
  };

  return (
    <Box display="flex" justifyContent="space-evenly" flexWrap="wrap">
      {categories.map((category) => (
        <Button id="category" key={category.name} style={{margin: "15px"}} onClick={() => handleClick(category.name)}>
          <Box display="flex" flexDirection="column" alignItems="center" padding='2%' color='rgb(86, 40, 131)' fontSize='1.1rem'>
            <img
              src={category.data}
              alt="category"
              style={{ height: "30vh", marginBottom: '7%' }}
            >
            </img>
            {category.displayName}
          </Box>
        </Button>
      ))}
    </Box>
  );
};
