import { useHistory } from "react-router-dom";

const useNav = () => {
  const history = useHistory();

  return (route: string) => {
    history.push(route);
  };
};

export default useNav;
