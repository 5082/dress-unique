import useUserData from "./useUserData";

export const useQueryData = ()=> {
    const { getUserChoices} = useUserData();
    const {color,gender,category} = getUserChoices();
    return {
        color: color || localStorage.getItem('color'),
        gender: gender || localStorage.getItem('gender'),
        category: category || localStorage.getItem('category')
    }
}