import { useState } from "react";
import UserContext from "./UserContext";
import { DesignChoices } from "../types/designSection";

const UserContextProvider = ({ children }: any) => {
  const [category, setCategory] = useState("");
  const [gender, setGender] = useState("");
  const [color, setColor] = useState("");

  const getUserChoices = (): DesignChoices => {
    return { category, gender, color };
  };

  const resetChoices = () => {
    setCategory("");
    setGender("");
    setColor("");
    localStorage.clear()
  };

  const setCategoryFeature = (value:string) =>{
    setCategory(value)
    localStorage.setItem('category',value)
  }

  const setColorFeature = (value:string) =>{
    setColor(value)
    localStorage.setItem('color',value)
  }

  const setGenderFeature = (value:string) =>{
    setGender(value)
    localStorage.setItem('gender',value)
  }

  return (
    <UserContext.Provider
      value={{
        setCategoryFeature,
        setGenderFeature,
        setColorFeature,
        getUserChoices,
        resetChoices,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContextProvider;
