import axios, { AxiosInstance } from "axios";

let instance: AxiosInstance|undefined = undefined

const conectToServer = () => {
    try {
        instance = axios.create({
            baseURL: "http://193.106.55.112:5000",
            timeout: 45000,
        });
    } catch (error) {
      console.log("could not connect to server", error);
    }
  };


const sendRequest = async(reqQuery:string) => {
    try{
        return await instance?.get(`/create?${reqQuery}`, {
            responseType: 'arraybuffer',
            timeout:45000
          });
    } catch( error){
        console.log("error",error)
    }   
}

export {sendRequest,conectToServer}
