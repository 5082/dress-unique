import Logo from './logo.png'
import Male from './male.png'
import Female from './female.png'
import ClothesImgs from './clothes'

const genderImgs = {Male,Female}

export {ClothesImgs,genderImgs,Logo}