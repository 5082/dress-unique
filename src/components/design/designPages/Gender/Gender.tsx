import { Box,Button } from "@mui/material"
import useUserData from "../../../../hooks/useUserData";
import { DesignSectionProps } from "../../../../types/designSection";
import { genderOptions } from "./genderOptions";
import './button.css';

export const Gender = ({saveChoice}:DesignSectionProps) => {
    const {setGenderFeature} = useUserData();
    
    const handleClick = (gender:string) => {
        setGenderFeature(gender)
        saveChoice('gender')
    }
    
    return (
        <Box display="flex" justifyContent="center" width="60vw">
            {genderOptions.map(gender => (
                <Button id="gender"
                        key={gender.name}
                        style={{
                        color: 'rgb(86, 40, 131)',
                        display:"flex",
                        flexDirection:"column",
                        fontSize:"1.1rem",
                        width: "27%",
                        margin: '3%' }}
                        onClick={(e)=>handleClick(gender.name)}>
                            <img
                            src={gender.data}
                            alt="category"
                            style={{ height: "40vh" }}
                        ></img>{gender.name}
                        </Button>))}
        </Box>
    )
}