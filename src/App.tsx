import { Home } from './components/home/Home';
import { Design } from './components/design/Design';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ResultWrapper } from './components/result/ResultWrapper';
import { Box } from '@mui/material';
import './App.css';
import { conectToServer } from './utils/serverInstance';

const routes = [
  {
    path: "/Home",
    component: Home,
  },
  {
    path: "/Design",
    component: Design,
  },
  {
    path: "/Result",
    component: ResultWrapper,
  },
];

const App = () => {
  conectToServer()
  
  return (
    <BrowserRouter>
      <Box className="App" display='flex' flexDirection="column">
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
            {routes.map(({ path, component: C }) => (
              <Route path={path} component={C} key={path} />
            ))}
          </Switch>
      </Box>
    </BrowserRouter>
  );
}

export default App;
