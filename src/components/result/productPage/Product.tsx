import { useState } from "react";
import { Box, Paper, Popover } from "@mui/material"
import DownloadIcon from '@mui/icons-material/Download';
import { DesignChoices } from "../../../types/designSection";

export type ProductType = {
    imageSrc:string;
    imageData:DesignChoices
}

export const Product = ({imageSrc,imageData}: ProductType) => {
    const [anchorEl, setAnchorEl] = useState<HTMLDivElement | null>(null);
    const {color,gender,category} = imageData
    const open = Boolean(anchorEl);
    const id = 'simple-popover';

    const handleOpenPopover = (event: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(event.currentTarget);
      };
    
    const handleClosePopover = () => {
        setAnchorEl(null);
    };

    const handleDownload = () => {
        const link = document.createElement('a');
        link.href = imageSrc;
        link.download = `${color}_${gender}_${category}.png`;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    return (
        <Box style={{alignSelf:"center", marginTop:"7vh"}}>
            <Box onClick={handleOpenPopover}>
                <Paper elevation={7} style={{width:"fit-content", padding:"10px", alignSelf:"center"}}>
                    <img src={imageSrc} alt="item" className="finalImage"/>
                </Paper>
            </Box>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClosePopover}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                 <Box sx={{p:1,cursor:"pointer", display:"flex",justifyContent:"center"}}  onClick={handleDownload}><DownloadIcon/>Download</Box>
            </Popover>
        </Box>
    )
    
}