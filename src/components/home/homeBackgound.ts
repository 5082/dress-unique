export const homeBackgound = () =>{
    const index = Math.floor(Math.random() * 23 + 1)
    const randomImg = `/homeBackgrounds/background${index}.png`

    return {
        backgroundImage :`url(${process.env.PUBLIC_URL + randomImg})`,
        backgroundRepeat: "no-repeat",
        backgroundSize:"100% 100%",
    }
} 