import { Box} from "@mui/material"
import { DesignSectionProps } from "../../../../types/designSection"
import useUserData from "../../../../hooks/useUserData";
import { ColorButton } from "./ColorButton";
import { colorOptions } from "./colorOptions";

export const Color = ({saveChoice}:DesignSectionProps) => {
    const {setColorFeature} = useUserData();
    
     const handleClick = (color:string) => {
        setColorFeature(color)
        saveChoice('color')
    }
   
    return (
    <Box display="flex" flexWrap="wrap" justifyContent="center" alignContent="center" width="60vw">
        {colorOptions.map(color => (<ColorButton key={color} color={color} onClick={handleClick}></ColorButton>))}        
    </Box>
    )
}