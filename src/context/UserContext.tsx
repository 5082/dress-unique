import { createContext } from "react";
import { DesignChoices } from "../types/designSection";

interface IUserContext {
  setCategoryFeature: (value:string) => void;
  setGenderFeature: (value:string) => void;
  setColorFeature: (value:string) => void;
  getUserChoices: () => DesignChoices;
  resetChoices: () => void;
}

const UserContext = createContext<IUserContext | undefined>(undefined);

export default UserContext;
