import { Box, Button } from "@mui/material"
import useNav from "../../hooks/useNav";
import { homeBackgound } from "./homeBackgound";

export const Home = () => {
    const redirect = useNav()
    const background = homeBackgound()

    const startDesign = () => {
        redirect('/Design')
    }

    return (
        <Box style={{
            ...background,
            display:"flex",
            flexDirection:"column-reverse",
            height:"-webkit-fill-available"}}>
            <Box style={{display:"flex", justifyContent:"center",
            width:"43%",
            marginBottom:"17vh"}} >
                <Button
                    onClick={startDesign}
                    style={{
                        backgroundColor: "black",
                        color: "white",
                        height: "10vh",
                        width: "17vh",
                        padding: '5%',
                        fontFamily: 'auto',
                        borderRadius: "50%",
                        fontSize:"1.4em"
                    }}
                >Start</Button>
            </Box>
        </Box>
    )
}