import { Box, Button} from "@mui/material";
import useNav from "../../../hooks/useNav";
import { createItem } from "../../../utils/createItem";
import { useState } from "react";
import { useQueryData } from "../../../hooks/useQueryData";
import useUserData from "../../../hooks/useUserData";
import { Product, ProductType } from "./Product";
import { LoadingPage } from "../LoadingPage";
import { getTextColorByUserColor } from "../../../common/colors";

type ImageSrc = Omit<ProductType,"imageData">

export const ProductPage = ({imageSrc}:ImageSrc) => {
  const [currImageSrc, setCurrImageSrc] = useState(imageSrc);
  const { resetChoices } = useUserData();
  const userChoices = useQueryData()
  const redirect = useNav();
  const pageHeaderStart = `Your one of a kind `;
  const pageHeaderEnd = ` ${userChoices.category} for ${userChoices.gender} is ready!`;
  const buttonStyle= {
    backgroundColor: "rgb(86 40 131)",
    color: "white",
    height: "10vh",
    width: "auto",
    fontFamily: "auto",
    padding: '2%',
    fontSize: "1em"
  }

  const handleRegenerate = async () => {
    setCurrImageSrc('');
    const src = await createItem(userChoices);
    setCurrImageSrc(src || '');
  };

  const handleDesignFromScratch = () => {
    resetChoices();
    redirect("/Design");
  };

  return (
    <Box display="flex" justifyContent="center" alignItems="center" flexDirection="column">
      <Box fontSize="30px" fontWeight="500" width="80%" alignSelf="center">
        <span>{pageHeaderStart}</span>
        <span style={{color: getTextColorByUserColor(userChoices.color)}}>{userChoices.color}</span>
        <span>{pageHeaderEnd}</span>
      </Box>
      {!!currImageSrc?<Product imageSrc={currImageSrc} imageData={userChoices}/>:<LoadingPage/>}
      <Box width='50%' display="flex" justifyContent="space-evenly" marginTop='7vh' marginBottom="5vh">
        <Button
          onClick={handleRegenerate}
          style={buttonStyle}
        >
          Regenerate
        </Button>
        <Button
          onClick={handleDesignFromScratch}
          style={buttonStyle}
        >
          Design from scratch
        </Button>
      </Box>
    </Box>
  );
};
