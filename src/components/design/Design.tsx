import { useState } from "react";
import { Box} from "@mui/material";
import { Gender } from "./designPages/Gender/Gender";
import { Color } from "./designPages/Color/Color";
import { Category } from "./designPages/Category/Category";
import { DesignSectionProps } from "../../types/designSection";
import useNav from "../../hooks/useNav";
import { Steps } from "./Steps";

const steps = [
  {
    title: "gender",
    label: "Choose Gender",
    comp: (props: DesignSectionProps) => <Gender {...props} />,
    isCompleted: false,
  },
  {
    title: "category",
    label: "Choose Category",
    comp: (props: DesignSectionProps) => <Category {...props} />,
    isCompleted: false,
  },
  {
    title: "color",
    label: "Choose Color",
    comp: (props: DesignSectionProps) => <Color {...props} />,
    isCompleted: false,
  },
  
];

export const Design = () => {
  const [activeStep, setActiveStep] = useState(0);
  const redirect = useNav();
  const stepsData = steps.map(step => ({title: step.title,label:step.label,isCompleted:step.isCompleted}))

  const finishDesign = () => {
    steps.forEach((step) => (step.isCompleted = false));
    redirect("/Result");
  };

  const saveChoice = (section: string) => {
    const currIndex = steps.findIndex((step) => step.title === section);
    steps[currIndex].isCompleted = true;
    const nextStep = currIndex + 1;
    nextStep === steps.length ? finishDesign() : setActiveStep(nextStep);
  };

  const CurrComp = () => {
    const Section = steps[activeStep].comp;
    return <Section saveChoice={saveChoice} />;
  };

  return (
    <Box style={{marginTop: '12vh'}}>
      <Steps steps={stepsData} activeStep={activeStep} setActiveStep={setActiveStep}/>
      <Box marginTop='10vh' display="flex" justifyContent="center">
        {CurrComp()}
      </Box>
    </Box>
  );
};
