import { ClothesImgs } from "../../../../assets";

const {TShirtImg, shirtImg, pantsImg, shortsImg, dressImg, jacketImg, closedShoesImg, openedShoesImg} = ClothesImgs

export const categoryOptions = [
  {
    name: "Shirts",
    displayName: "Shirts",
    data: TShirtImg,
    genders: ['Male', 'Female'],
    subCategories: [
      {
        name: "TShirt",
        displayName: "TShirt",
        data: TShirtImg,
        genders: ['Male', 'Female']
      },
      {
        name: "Shirt",
        displayName: "Shirt",
        data: shirtImg,
        genders: ['Male', 'Female']
      }
    ]
  },
  {
    name: "Pants",
    displayName: "Pants",
    data: pantsImg,
    genders: ['Male', 'Female'],
    subCategories: [
      {
        name: "Pants",
        displayName: "Pants",
        data: pantsImg,
        genders: ['Male', 'Female']
      },
      {
        name: "Shorts",
        displayName: "Shorts",
        data: shortsImg,
        genders: ['Male', 'Female']
      }
    ]
  },
  {
    name: "Dresses",
    displayName: "Dress",
    data: dressImg,
    genders: ['Female'],
  },
  {
    name: "Jackets",
    displayName: "Jacket",
    data: jacketImg,
    genders: ['Male', 'Female']
  },
  {
    name: "Shoes",
    displayName: "Shoes",
    data: closedShoesImg,
    genders: ['Male', 'Female'],
    subCategories: [
      {
        name: "Closed Shoes",
        displayName: "Closed Shoes",
        data: closedShoesImg,
        genders: ['Male', 'Female']
      },
      {
        name: "Opened Shoes",
        displayName: "Opened Shoes",
        data: openedShoesImg,
        genders: ['Male', 'Female']
      }
    ]
  },
];