export type DesignSectionProps = {
  saveChoice: any;
};

export type DesignChoices = {
  color: string|null;
  category: string|null;
  gender: string|null;
};

export type StepType = {
  title: string;
  label:string;
  isCompleted:boolean;
}

export type StepsType = {
    steps: StepType[];
    activeStep:number;
    setActiveStep: (index:number)=> void
}
