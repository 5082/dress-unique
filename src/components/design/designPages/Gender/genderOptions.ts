import { genderImgs } from "../../../../assets";

const { Female, Male } = genderImgs

export const genderOptions = [
    {
      name: "Male",
      data: Male,
    },
    {
      name: "Female",
      data: Female,
    }
  ];