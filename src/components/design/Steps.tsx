import { Step, StepButton, StepLabel, StepIcon, Stepper } from "@mui/material";
import { StepsType } from "../../types/designSection";

export const Steps = ({steps,activeStep,setActiveStep}:StepsType) => {

  const onClickStep = (index: number) => {
    if(index < activeStep)  {
      steps.forEach((step,i) => {
        if(i > index){
          steps[i].isCompleted = false
        }
      })
      setActiveStep(index)
    }
  }

  return (
    <Stepper
      style={{ width: "65%" , marginInline:'auto'}}
      activeStep={activeStep}
      nonLinear
    >
      {steps.map((section,index) => (
        <Step
        key={section.title}
        style={{
          backgroundColor: "rgb(86 40 131)",
          padding: "0.5rem",
          borderRadius: "1rem",
        }}
        completed={section.isCompleted}
      >
        <StepButton
          sx={{
            "& .MuiStepLabel-label": { color: "white", fontSize: "1.1em" },
            "& .MuiStepLabel-label.Mui-active": { color: "white" },
          }}
          onClick={()=>onClickStep(index)}
          >
          <StepLabel StepIconComponent={
            (props)=>(
              <StepIcon {...props}
                sx={{"&.MuiStepIcon-root.Mui-active": {color:"#7861cd"},
                "&.MuiStepIcon-root.Mui-completed": {color:"#7861cd"}}}/>)}>
          {section.label}
        </StepLabel>
        </StepButton>
      </Step>
      ))}
    </Stepper>    
  )
}