export const getTextColorByUserColor = (color: string | null) => {
    const headlineColor = color || 'black';
    if (headlineColor.toLocaleLowerCase() === 'white') {
      return 'Black';
    }
    if (headlineColor.toLocaleLowerCase() === 'pink') {
      return 'deeppink';
    }
    return headlineColor;
}

export const getBackgroundColorByColor = (color: string) => {
    if (color.toLocaleLowerCase() === 'pink') {
        return 'deeppink';
    }
    return color;
} 