import TShirtImg from './shirts/TShirt.png';
import shirtImg from './shirts/shirt.png';
import pantsImg from './pants/pants.png';
import shortsImg from './pants/shorts.png';
import dressImg from './dress.png';
import jacketImg from './jacket.png';
import closedShoesImg from './shoes/closedShoes.png';
import openedShoesImg from './shoes/openedShoes.png';

const ClothesImgs = {TShirtImg, shirtImg, pantsImg, shortsImg, dressImg, jacketImg, closedShoesImg, openedShoesImg};

export default ClothesImgs;