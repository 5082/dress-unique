import { Box, Card, CardActionArea, CardContent } from "@mui/material"
import { getBackgroundColorByColor, getTextColorByUserColor } from "../../../../common/colors"
import './button.css';

type ColorButtonProps = {
    color: string,
    onClick: any
}

export const ColorButton = ({ color, onClick }: ColorButtonProps) => {
    return (
        <Box id={'color-' + color.toLocaleLowerCase()} style={{padding:"2%", margin: '1%', width: '15%'}} sx={{borderRadius: '10%', minWidth: '100px'}} onClick={()=> onClick(color)}>
            <Card elevation={5}>
                <CardActionArea sx={{
                    "& .MuiCardContent-root": { fontSize: "1.1em", padding: "7px" },
                }}>
                    <Box style={{ backgroundColor: getBackgroundColorByColor(color), height: "60px" }} />
                    <CardContent>
                        <Box component="div" textAlign="center" alignContent="center" color={getTextColorByUserColor(color)}>
                            {color.toUpperCase()}
                        </Box>
                    </CardContent>
                </CardActionArea>
            </Card></Box>
    )
}