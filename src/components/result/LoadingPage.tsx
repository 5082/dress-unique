import { Box, CircularProgress } from "@mui/material"

export const LoadingPage = () => {
    return (
        <Box sx={{ display: 'flex', justifyContent:"center", marginTop:"7vh"}}>
            <CircularProgress size='250px' style={{color: 'rgb(86, 40, 131)'}}/>
        </Box>
    )
}