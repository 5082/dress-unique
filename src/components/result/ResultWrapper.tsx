import { Box } from "@mui/material"
import { createItem } from "../../utils/createItem";
import { useEffect, useState } from "react";
import { ProductPage } from "./productPage/ProductPage";
import { useQueryData } from "../../hooks/useQueryData";
import { LoadingPage } from "./LoadingPage";
import { Logo } from "../../assets";

export const ResultWrapper = () => {
    const [imageSrc, setImageSrc] = useState('');
    const dataForModel = useQueryData()

    useEffect(() => {
        const fetchImage = async () => {
            const src = await createItem(dataForModel);
            setImageSrc(src || '');        
        };
    
        fetchImage();
    },[]);
    
    return (
        <>
            <img src={Logo} className="App-logo" alt="logo" style={{position: "absolute", height: '15vh', width: '15vh', margin: '1%', pointerEvents: 'none'}} />
            <Box style={{paddingTop: '15vh'}}>
                {imageSrc === '' ? <LoadingPage/>:<ProductPage imageSrc={imageSrc}/>}
            </Box>
        </>
    )
}

// .App-logo {
//     position: absolute;
//     height: 15vh;
//     width:15vh;
//     pointer-events: none;
//     margin-top: 5px;
//     margin-left: 5px;
//   }
  