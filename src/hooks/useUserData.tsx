import { useContext } from "react";
import UserContext from "../context/UserContext";

const useUserData = () => {
  const context = useContext(UserContext);

  if (!context) {
    throw Error("user context is not defined here");
  }

  return context;
};

export default useUserData;
